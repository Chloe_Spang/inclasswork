/**********************************************
* File: RBTTest.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
*  
**********************************************/
#include <iostream>
#include "RedBlackTree.h"
using namespace std;

    // Test program
/********************************************
* Function Name  : main
* Pre-conditions :  
* Post-conditions: int
*  
********************************************/
int main( )
{
    const int NEG_INF = -9999;
    RedBlackTree<int> t{ NEG_INF };
    int NUMS = 400000;
    const int GAP  =   37;
    int i;

    std::cout << "Checking... (no more output means success)" << std::endl;

    for( i = GAP; i != 0; i = ( i + GAP ) % NUMS )
        t.insert( i );

    if( NUMS < 40 )
        t.printTree( );
    if( t.findMin( ) != 1 || t.findMax( ) != NUMS - 1 )
        std::cout << "FindMin or FindMax error!" << std::endl;

    for( i = 1; i < NUMS; ++i )
        if( !t.contains( i ) )
            std::cout << "Find error1!" << std::endl;
    if( t.contains( 0 ) )
        std::cout << "Oops!" << std::endl;

    
    RedBlackTree<int> t2{ NEG_INF };
    t2 = t;

    for( i = 1; i < NUMS; ++i )
        if( !t2.contains( i ) )
            std::cout << "Find error1!" << std::endl;
    if( t2.contains( 0 ) )
        std::cout << "Oops!" << std::endl;

    std::cout << "Test complete..." << std::endl;
    return 0;
}
