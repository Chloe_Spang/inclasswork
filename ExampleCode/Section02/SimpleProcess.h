/*******************************
 * File name: SimpleProcess.h 
 * Author: Matthew Morrison
 * E-mail: matt.morrison@nd.edu 
 * 
 * This file contains a simple process 
 * definition to implement a 
 * Priority Queue.
 * ****************************/

#ifndef SIMPLEPROCESS_H
#define SIMPLEPROCESS_H

#include <cstdlib>

class SimpleProcess{
    
    public:


        /************************************
        * Function Name: SimpleProcess()
        * Preconditions: none  
        * Postconditions: none  
        * Empty Constructor. Low Priority of 40
        * ***********************************/  
        SimpleProcess() {
            std::string temp (40, 'Z');
            processString = temp;
            priorityVal = processString.length();
        }

        /************************************
        * Function Name: SimpleProcess()
        * Preconditions: std::string  
        * Postconditions: none  
        * Constructor
        * ***********************************/         
        SimpleProcess(std::string inputString) {
            processString = inputString;
            priorityVal = processString.length();
        }
 
         /************************************
        * Function Name: getPriority
        * Preconditions: none  
        * Postconditions: const size_t  
        * Returns the priority Value
        * ***********************************/        
        const size_t getPriority(){
            return priorityVal;
        }

         /************************************
        * Function Name: ageProcess
        * Preconditions: size_t  
        * Postconditions: none  
        * Decrements the priority value, which 
        * increases the actual priority 
        * ***********************************/          
        void ageProcess(size_t age_decr){
            if(priorityVal < age_decr){
                priorityVal = 1;
            }
            else{
                priorityVal -= age_decr;
            }
        }

         /************************************
        * Function Name: getProcess
        * Preconditions: none  
        * Postconditions: std::string  
        * Returns the process String 
        * ***********************************/            
        std::string getProcess(){
            return processString;
        }
 
       /************************************
        * Function Name: operator<
        * Preconditions: const SimpleProcess&, const SimpleProcess&   
        * Postconditions: bool  
        * This friend function compares the priority of the two processes
        * If left.priorityVal > right.priorityVal, then the left has the 
        * higher priorityVal, and this the lower priority
        * ***********************************/        
        friend bool operator< (const SimpleProcess& left, const SimpleProcess& right){
            return left.priorityVal > right.priorityVal;
        }
 
        /************************************
        * Function Name: operator>
        * Preconditions: const SimpleProcess&, const SimpleProcess&   
        * Postconditions: bool  
        * This friend function compares the priority of the two processes
        * If left.priorityVal < right.priorityVal, then the left has the 
        * lower priorityVal, and this the higher priority
        * ***********************************/         
        friend bool operator> (const SimpleProcess& left, const SimpleProcess& right){
            return left.priorityVal < right.priorityVal;
        }

        /************************************
        * Function Name: operator==
        * Preconditions: const SimpleProcess&, const SimpleProcess&   
        * Postconditions: bool  
        * This friend function compares == the priority of the two processes
        * ***********************************/          
        friend bool operator== (const SimpleProcess& left, const SimpleProcess& right){
            return left.priorityVal == right.priorityVal;
        }

        /************************************
        * Function Name: operator!=
        * Preconditions: const SimpleProcess&, const SimpleProcess&   
        * Postconditions: bool  
        * This friend function compares != the priority of the two processes
        * ***********************************/          
        friend bool operator!= (const SimpleProcess& left, const SimpleProcess& right){
            return left.priorityVal != right.priorityVal;
        }     

        /************************************
        * Function Name: operator<<
        * Preconditions: std::ostream&, const SimpleProcess&    
        * Postconditions: bool  
        * This friend function overloads the ostream operator
        * ***********************************/          
        friend std::ostream& operator<< (std::ostream& stream, const SimpleProcess& pr){
         
            stream << "Priority: " << pr.priorityVal << "\tProcess: " << pr.processString;
         
            return stream;   
        }
    
    private:
        std::string processString;
        size_t priorityVal;
};

#endif